package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.impl.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.repository.impl.CollectionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
 class CollectionRepositoryTest {

	private final CollectionRepository collectionRepository = new CollectionRepositoryImpl();
	private final CollectionItemRepository itemRepository = new CollectionItemRepositoryImpl();


	@BeforeEach
	void setUp(){
		itemRepository.deleteAll();
		collectionRepository.deleteAll();
	}

	@Test
	void save_collectionWithoutItem() {

		//given
		Collection c = Collection.builder()
				.name("asd")
				.title("qwe")
				.imageUrl("sdf")
				.build();

		//when
		Collection saved = collectionRepository.create(c);

		//then
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void save_collectionWithItem() {

		//given

		Collection c = Collection.builder()
				.name("asd")
				.title("qwe")
				.imageUrl("sdf")
				.description("yft")
				.build();

		//when
		Collection saved = collectionRepository.create(c);


		CollectionItem i1 = CollectionItem.builder().name("i1").collection(c).build();
		CollectionItem i2 = CollectionItem.builder().name("i2").collection(c).build();
		List<CollectionItem> items = new ArrayList<>();
		items.add(i1);
		items.add(i2);

		CollectionItem savedI1 = itemRepository.create(i1);
		CollectionItem savedI2 = itemRepository.create(i2);

		List<CollectionItem> itemsList = new ArrayList<>();
		itemsList.add(savedI1);
		itemsList.add(savedI2);



		//then
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void findById_happyPath () {

		//given
		Collection c = Collection.builder()
				.name("asd")
				.title("qwe")
				.imageUrl("sdf")
				.build();

		//when
		Collection saved = collectionRepository.create(c);


		CollectionItem i1 = CollectionItem.builder().name("item 1").collection(saved).build();
		CollectionItem i2 = CollectionItem.builder().name("item 2").collection(saved).build();


		itemRepository.create(i1);
		itemRepository.create(i2);

		//when

		Collection found = collectionRepository.findById(saved.getId());

		//then
		Assertions.assertNotNull(found);
		Assertions.assertNotNull(found.getId());
		Assertions.assertNotNull(found.getItems());
		Assertions.assertEquals(2,found.getItems().size());
	}

}
