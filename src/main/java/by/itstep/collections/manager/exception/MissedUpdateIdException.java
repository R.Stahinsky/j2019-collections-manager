package by.itstep.collections.manager.exception;

public class MissedUpdateIdException extends InvalidDtoException{

    public MissedUpdateIdException(final String message) {
        super(message);
    }
}
