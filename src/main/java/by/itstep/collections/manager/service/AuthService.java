package by.itstep.collections.manager.service;
import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.enums.Role;

public interface AuthService {

    void login(UserLoginDto loginDto);

    void logout();

    boolean isAuthenticated();

    Role getRole();

    User getLoginedUser();
}
