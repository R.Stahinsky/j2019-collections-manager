package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.entity.User;

import by.itstep.collections.manager.enums.Role;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.impl.UserRepositoryImpl;
import by.itstep.collections.manager.service.AuthService;
import by.itstep.collections.manager.service.UserService;

import java.util.HashMap;
import java.util.Map;

public class AuthServiceImpl implements AuthService {

    private User loginedUser;
    private final UserService userService = new UserServiceImpl();
    private final UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public void login(final UserLoginDto loginDto) {
        User foundUser = userRepository.findByEmail(loginDto.getEmail());//TODO add to service

        if (foundUser == null){
            throw new RuntimeException("User not found by email: " + loginDto.getEmail());
        }
        if (!foundUser.getPassword().equals(loginDto.getPassword())){
            throw new RuntimeException("User password is incorrect!");
        }
        loginedUser = foundUser;
    }

    @Override
    public void logout() {
        loginedUser = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginedUser != null;
    }


    @Override
    public Role getRole() {
        return loginedUser.getRole();
    }

    @Override
    public User getLoginedUser() {
        return loginedUser;
    }
}
//public class AuthServiceImpl implements AuthService{
//    private Map<String, User> authContext = new HashMap<>();
//    private UserRepository userRepository = new UserRepositoryImpl();
//    @Override
//    public void login(UserLoginDto userLoginDto) {
//        User foundUser=userRepository.findByEmail(userLoginDto.getEmail());
//        if(foundUser == null){
//            throw new RuntimeException("User not foud by email: " + userLoginDto.getEmail());
//        }
//        if(!foundUser.getPassword().equals(userLoginDto.getPassword())){
//            throw new RuntimeException("Incorrect password");
//        }
//        authContext.put(foundUser.getEmail(), foundUser);
//    }
//
//    @Override
//    public void logout(String email) {
//        if(isAuthenticated(email)){
//            authContext.remove(email);
//        }
//    }
//
//    @Override
//    public boolean isAuthenticated(String email) {
//        if(authContext.containsKey(email)){
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public Long getAuthenticatedId(String email) {
//        if(isAuthenticated(email)){
//            return authContext.get(email).getId();
//        }
//        return null;
//    }
//}
