package by.itstep.collections.manager.dto.comment;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
public class CommentFullDto {

    private Long id;
    private String message;
    private User user;
    private Collection collection;
    private Date createdAt;
}
