package by.itstep.collections.manager.dto.tag;

import by.itstep.collections.manager.entity.Collection;
import lombok.Data;

import java.util.List;

@Data
public class TagFullDto {

    private Long id;
    private String name;
    private List<Collection> collections;
}
