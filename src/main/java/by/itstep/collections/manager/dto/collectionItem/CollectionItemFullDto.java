package by.itstep.collections.manager.dto.collectionItem;

import by.itstep.collections.manager.entity.Collection;
import lombok.Data;

@Data
public class CollectionItemFullDto {

    private Long id;
    private Collection collection;
    private String name;
}
