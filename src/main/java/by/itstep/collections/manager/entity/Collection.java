package by.itstep.collections.manager.entity;

import lombok.*;

import javax.persistence.*;

import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "collection")
public class Collection {

    @Id// primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //auto incriment
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "img_url")
    private String imageUrl;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "collection")
    private List<CollectionItem> items;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "collection",fetch = FetchType.EAGER)
    private List<Comment> comments;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "collection-tag",
            joinColumns ={@JoinColumn(name = "collection_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
            )
    private List<Tag> tags;


}

